**HACKATHON PROJECT**

**Team DUBLIN DG 1**

*Team Members:*

1. Karishma Tyagi
2. Kriti Dixit
3. Pooja Narke

**TASK:**

Train either a linear regression or a classification ML model based on some open source financial or economic data. This is a very open task, you may choose any data that is in anyway related to financial activity or the finance sector.

a. An example of this could be: You think there is potentially a correlation between some macro economic factors, or that macro economic factors are correlated with other trends. You would then look for data gathered on those factors (e.g. what would influence consumer inflation?). You can then quickly plot the data to see if there is a trend. If there is, you could use that data to train a model so that predictions can be made in future on those trends.

b. You may expand this task by saving your resulting model file (e.g. using joblib). You can then create a flask REST application that will load that joblib file. The REST application can then ask the model for a prediction when it receives incoming HTTP requests.

**METHODOLOGY**

1. Revision Control: BitBucket Repository Setup

2. Project Management Tool : Trello Setup

3. Data Collection: get_data_yahoo-> Citi Stocks

4. Data Preprocessing:

	a. Null Values Check
	
	b. Descriptive Statistics

5. Visual Analysis

6. Model

	[LinearRegression](https://bitbucket.org/k_tyagi12/dub_dg1_flask/src/master/Lr.jpg)
	
	
	a. Model Build
	
	b. Model Training
	
	c. Predictions

7. Performance Evaluation

8. Flask REST API:
	
	a. Input: Ticker and Date
	
	b. Input Date
